#include "Mesh.h"

Mesh::Mesh(const Material& a_Material, bool canSelfShade)
	: RenderObject(a_Material), m_CanSelfShade(canSelfShade)
{}

Mesh::~Mesh()
{}

void Mesh::AddVertex(double x, double y, double z)
{
	if (m_Vertices.size() < 1)
	{
		m_Minima(0) = x;
		m_Minima(1) = y;
		m_Minima(2) = z;
		m_Maxima(0) = x;
		m_Maxima(1) = y;
		m_Maxima(2) = z;
	}

	m_Vertices.push_back(Vertex(Vector3d(x, y, z)));
	UpdateBounds(x, y, z);
}

void Mesh::AddFace(int v1, int v2, int v3)
{
	Triangle newFace(&m_Vertices[v1], &m_Vertices[v2], &m_Vertices[v3]);
	m_Faces.push_back(newFace);
	m_Vertices[v1].AddFaceNormal(newFace.GetNormal());
	m_Vertices[v2].AddFaceNormal(newFace.GetNormal());
	m_Vertices[v3].AddFaceNormal(newFace.GetNormal());
}

void Mesh::UpdateBounds(double x, double y, double z)
{
	m_Minima(0) = min(x, m_Minima(0));
	m_Minima(1) = min(y, m_Minima(1));
	m_Minima(2) = min(z, m_Minima(2));

	m_Maxima(0) = max(x, m_Maxima(0));
	m_Maxima(1) = max(y, m_Maxima(1));
	m_Maxima(2) = max(z, m_Maxima(2));
}

void Mesh::CreateBoundingSphere()
{
	if (m_BoundingSphere != nullptr)
	{
		delete m_BoundingSphere;
	}

	auto distance = m_Maxima - m_Minima;
	double radius = distance.norm() * 0.5;

	Vector3d center = (m_Maxima + m_Minima) * 0.5;
	m_BoundingSphere = new Sphere(center, radius);
}

void Mesh::CreateAABB()
{
	if (m_AABB != nullptr)
	{
		delete m_AABB;
	}

	m_AABB = new AABB(m_Maxima, m_Minima);
}

TraceResult Mesh::Intersect(Ray & a_Ray)
{
	TraceResult result;
	result.Hit = Miss;

	// Default to true so the mesh triangles will be checked if no bounding
	// object was created.
	bool boundingIntersection = true;
	if (m_AABB != nullptr)
	{
		boundingIntersection = m_AABB->Intersect(a_Ray);
	}
	else if (m_BoundingSphere != nullptr)
	{
		if (m_CanSelfShade)
			boundingIntersection = m_BoundingSphere->Intersect(a_Ray).Hit != Miss;
		else
		{
			if (a_Ray.m_SpawnObject == this)
				return result;

			boundingIntersection = m_BoundingSphere->Intersect(a_Ray).Hit == Hit;
		}
	}

	if (boundingIntersection)
	{
		for (size_t i = 0; i < m_Faces.size(); i++)
		{
			TraceResult faceResult = m_Faces[i].Intersect(a_Ray);

			if (faceResult.Hit == Hit)
			{
				if (result.Hit == Miss || faceResult.Distance < result.Distance)
				{
					result = faceResult;
				}
			}
		}

		result.Material = &m_Material;

		if (!m_CanSelfShade)
			result.Object = this;
	}

	return result;
}

string Mesh::Save()
{
	return "";
}