#pragma once

#include "RenderObject.h"

class Sphere : public RenderObject
{
private:
	Vector3d m_Center;
	double m_Radius;

public:

	Sphere(
		const Vector3d & a_Center = Vector3d(),
		double a_Radius = 1,
		const Material & a_Material = Material()
		) :
		RenderObject(a_Material),
		m_Center(a_Center),
		m_Radius(a_Radius)
	{}
	~Sphere() {}

	void SetTextureCoordinates(TraceResult & a_Result);
	string Save();
	TraceResult Intersect(Ray & a_Ray);
};