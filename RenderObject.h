#pragma once

#include "Material.h"
#include "Ray.h"
#include <Eigen/Dense>
using namespace Eigen;

struct TraceResult;

class RenderObject
{
protected:
	Material m_Material;

public:
	RenderObject(const Material & a_Material = Material())
		:m_Material(a_Material)
	{}

	virtual ~RenderObject() {}

	const Material & GetMaterial()
	{
		return m_Material;
	}
	virtual TraceResult Intersect(Ray & a_Ray) = 0;
	virtual string Save() = 0;
};

enum HitResult
{
	Inside = -1,
	Miss = 0,
	Hit = 1
};

struct TraceResult
{
	HitResult Hit = Miss;
	double Distance;
	Vector3d Intersection;
	Material* Material;
	Vector3d Normal;
	TextureCoordinate TextureCoordinate;
	RenderObject* Object = nullptr;
};