#ifndef RANDOM_H_
#define RANDOM_H_
//generates a psuedo-random double between 0.0 and 0.999...
double randdouble()
{
    return rand()/(double(RAND_MAX)+1);
}

//generates a psuedo-random double between 0.0 and max
double randdouble(double max)
{
    return randdouble()*max;
}

//generates a psuedo-random double between min and max
double randdouble(double min, double max)
{
    if (min>max)
    {
        return randdouble()*(min-max)+max;    
    }
    else
    {
        return randdouble()*(max-min)+min;
    }
} 
#endif /*RANDOM_H_*/
