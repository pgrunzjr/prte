#include "Plane.h"

double Plane::GetOffset() { return m_Offset; }

TraceResult Plane::Intersect(Ray & a_Ray)
{
	TraceResult a_Result;
	a_Result.Hit = Miss;

	if (a_Ray.m_SpawnObject == this)
		return a_Result;

	//Take the dot product of the ray direction, and the vector distance
	//from the ray origin to the sphere center;
	Vector3d QE = (m_Normal * m_Offset) - a_Ray.m_Origin;
	double numer = QE.dot(m_Normal);
	double denom = a_Ray.m_Direction.dot(m_Normal);

	//If 0, Plane is parallel to ray
	if (denom != 0)
	{
		double t = numer / denom;

		//If t < 0, intersection is behind ray origin and we ignore it
		if (t >= 0)
		{
			a_Result.Hit = Hit;
			a_Result.Distance = t;
			a_Result.Intersection = a_Ray.m_Origin + (t * a_Ray.m_Direction);
			a_Result.Normal = m_Normal;
			a_Result.Material = &m_Material;
			a_Result.Object = this;
			SetTextureCoordinates(a_Result);
		}
	}

	return a_Result;
}

string Plane::Save()
{
	stringstream out;
	out << "[PLANE]" << endl
		<< m_Normal.x() << ','
		<< m_Normal.y() << ','
		<< m_Normal.z() << ','
		<< m_Offset << ','
		<< m_Material.GetColor().R << ','
		<< m_Material.GetColor().G << ','
		<< m_Material.GetColor().B << ','
		<< m_Material.GetDiffuse() << ','
		<< m_Material.GetSpecular() << ','
		<< m_Material.GetSpecFalloff() << ','
		<< m_Material.GetReflectivity()
		<< endl << "[/PLANE]" << endl;

	return out.str();
}

void Plane::SetTextureCoordinates(TraceResult & a_Result)
{
	Texture texture = m_Material.GetTexture();

	//Convert intersect coords to texture coords
	Vector3d pos = a_Result.Intersection;// - m_Normal;
	double u = pos.dot(m_UAxis) * texture.m_XScale;
	double v = pos.dot(m_VAxis) * texture.m_YScale;

	double uRot = u*cos(texture.m_Rotation) - v*sin(texture.m_Rotation);
	v = v*cos(texture.m_Rotation) + u*sin(texture.m_Rotation);
	u = uRot + texture.m_Origin.x();
	v = v + texture.m_Origin.y();

	a_Result.TextureCoordinate.u = u;
	a_Result.TextureCoordinate.v = v;
}

Vector3d Plane::GetNormal(TraceResult & a_Result)
{
	if (m_Normal.x() == 0 && m_Normal.y() == 0 && m_Normal.z() == 1)
	{
		Texture normal_map = m_Material.GetNormalMap();
		if (normal_map.m_Image)
		{
			Color normal_color;

			//Convert intersect coords to texture coords
			double u = a_Result.Intersection.dot(m_UAxis) * normal_map.m_XScale;
			double v = a_Result.Intersection.dot(m_VAxis) * normal_map.m_YScale;

			normal_color = normal_map.GetBilerpPixColor(u, v);

			return Vector3d(-2.0 * (normal_color.R - 0.5), 2.0 * (normal_color.G - 0.5), 2.0 * (normal_color.B - 0.5)).normalized();
		}
	}

	return m_Normal;
}

double Plane::GetSpecular(TraceResult & a_Result)
{
	Texture specular_map = m_Material.GetSpecularMap();
	if (specular_map.m_Image)
	{
		Color specular_color;

		//Convert intersect coords to texture coords
		double u = a_Result.Intersection.dot(m_UAxis) * specular_map.m_XScale;
		double v = a_Result.Intersection.dot(m_VAxis) * specular_map.m_YScale;

		specular_color = specular_map.GetBilerpPixColor(u, v);

		return (specular_color.R + specular_color.G + specular_color.B) / 3.0;
	}

	return m_Material.GetSpecular();
}