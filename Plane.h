#pragma once

#include "RenderObject.h"

class Plane : public RenderObject
{
private:
	double m_Offset;
	Vector3d m_UAxis, m_VAxis;
	Vector3d m_Normal;

public:
	Plane(
		const Vector3d & a_Normal = Vector3d(0, 1, 0),
		double a_Offset = 0,
		const Material & a_Material = Material()
		) :
		RenderObject(a_Material),
		m_Offset(a_Offset)
	{
		m_Normal = a_Normal;

		//z = neg x
		m_UAxis.x() = -m_Normal.z();
		m_UAxis.y() = m_Normal.x();
		m_UAxis.z() = m_Normal.y();
		m_VAxis = m_UAxis.cross(m_Normal);
	}

	~Plane() {}

	double GetOffset();
	void SetTextureCoordinates(TraceResult & a_Result);

	// TODO: The calculations done in these two methods should be moved to the raytracer Shade method.
	Vector3d GetNormal(TraceResult & a_Result);
	double GetSpecular(TraceResult & a_Result);

	string Save();
	TraceResult Intersect(Ray & a_Ray);
};