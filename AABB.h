#pragma once

#include "Ray.h"
#include <Eigen\Dense>
using namespace Eigen;

class AABB
{
private:
	Vector3d m_Maxima = Vector3d(0.0, 0.0, 0.0);
	Vector3d m_Minima = Vector3d(0.0, 0.0, 0.0);

public:
	AABB();
	AABB(Vector3d a_Maxima, Vector3d a_Minima);
	~AABB();

	bool Intersect(const Ray& a_Ray);
};