/*
 * color.cpp
 */

#include "Color.h"

Color operator*(const Color& c, double d) {
	Color ret_val;
	ret_val.R = c.R * d;
	ret_val.G = c.G * d;
	ret_val.B = c.B * d;

	return ret_val;
}
Color operator*(double d, const Color& c) {
	return c * d;
}
Color operator*(const Color& c1, const Color& c2)
{
	Color ret_val;

	ret_val.R = c1.R * c2.R;
	ret_val.G = c1.G * c2.G;
	ret_val.B = c1.B * c2.B;

	return ret_val;
}
Color operator+(const Color& c1, const Color& c2) {
	Color ret_val;

	ret_val.R = c1.R + c2.R;
	ret_val.G = c1.G + c2.G;
	ret_val.B = c1.B + c2.B;

	return ret_val;
}
Color operator/(const Color& c, double d) {
	Color ret_val;
	ret_val.R = c.R / d;
	ret_val.G = c.G / d;
	ret_val.B = c.B / d;

	return ret_val;
}

ostream& operator<<(ostream & out, Color & color) {
	out << "R:" << color.R
	    << "G:" << color.G
	    << "B:" << color.B;

	return out;
}
