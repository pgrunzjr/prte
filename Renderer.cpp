#include <cmath>
#include <omp.h>
#include "Renderer.h"
#include "Raytracer.h"
#include "RayGenerator.h"

void UpdateScreen(SDL_Texture* a_Texture, SDL_Renderer* a_Renderer, Uint32* a_Pixels, int a_Pitch)
{
	SDL_UpdateTexture(a_Texture, NULL, a_Pixels, a_Pitch);
	SDL_RenderClear(a_Renderer);
	SDL_RenderCopy(a_Renderer, a_Texture, NULL, NULL);
	SDL_RenderPresent(a_Renderer);
}

void Renderer::Render(const Scene & a_Scene, SDL_Renderer* a_Renderer)
{
	RayGenerator rayGenerator = RayGenerator(m_ScreenWidth, m_ScreenHeight, m_Samples, a_Scene);
	RayTracer rayTracer = RayTracer(m_Samples, a_Scene);

	Uint32 ticks = SDL_GetTicks();
	unsigned int num_pix = m_ScreenHeight * m_ScreenWidth;
	unsigned int num_samples = m_Samples * m_Samples;
	Uint32* pixels = new Uint32[num_pix];

	SDL_Texture* sdlTexture = SDL_CreateTexture(a_Renderer,
		SDL_PIXELFORMAT_RGB888,
		SDL_TEXTUREACCESS_STREAMING,
		m_ScreenWidth, m_ScreenHeight);

	for (int y = 0; y < m_ScreenHeight; ++y) {
		#pragma omp parallel for schedule(dynamic)
		for (int x = 0; x < m_ScreenWidth; x++) {
			Ray* cur_ray = rayGenerator.GenerateRays(x, y);
			Color cur_color = rayTracer.Trace(cur_ray) / num_samples;
			delete[] cur_ray;

			double max = 1.0;
			double min = max - 1.0;
			if (cur_color.R > max) {
				cur_color.R = 1;
			}
			else if (cur_color.R < min) {
				cur_color.R = 0;
			}
			else {
				cur_color.R = cur_color.R - min;
			}

			if (cur_color.G > max) {
				cur_color.G = 1;
			}
			else if (cur_color.G < min) {
				cur_color.G = 0;
			}
			else {
				cur_color.G = cur_color.G - min;
			}

			if (cur_color.B > max) {
				cur_color.B = 1;
			}
			else if (cur_color.B < min) {
				cur_color.B = 0;
			}
			else {
				cur_color.B = cur_color.B - min;
			}

			Uint32* cur_pix = pixels + (y * m_ScreenWidth) + x;
			*cur_pix = (Uint32(255) << 24) | (Uint32(cur_color.R * 255) << 16) | (Uint32(cur_color.G * 255) << 8) | Uint32(cur_color.B * 255);
		}

		if ((SDL_GetTicks() - ticks) > 1000)
		{
			ticks = SDL_GetTicks();
			UpdateScreen(sdlTexture, a_Renderer, pixels, m_ScreenWidth * sizeof(Uint32));
		}
	}

	UpdateScreen(sdlTexture, a_Renderer, pixels, m_ScreenWidth * sizeof(Uint32));
}