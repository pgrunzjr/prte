/*
 * scene.h
 *
 *  Created on: Aug 2, 2008
 *      Author: phildo
 */

#ifndef SCENE_H_
#define SCENE_H_

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "Light.h"
#include "RenderObject.h"
#include "Ray.h"

class Scene
{
private:
	bool m_Ready;

public:
	vector<Light> m_Lights;
	vector<RenderObject*> m_Primitives;
	Camera m_Camera;
	bool m_DisplayLights;

	Scene() {}
	Scene(string a_Filename)
	{
		if (Load(a_Filename))
		{
			m_Ready = true;
		}
	}
	~Scene() {}

	bool Load(string a_Filename);
	// TODO I don't like this...
	bool LoadPLY(ifstream& a_File);
	void Save(string a_Filename);
	vector<string> Explode(string str, char delim);
};

#endif /* SCENE_H_ */
