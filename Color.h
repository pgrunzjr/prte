/*
 * color.h
 *
 *  Created on: Aug 2, 2008
 *      Author: phildo
 */

#ifndef COLOR_H_
#define COLOR_H_

#include <iostream>
using namespace std;

class Color {
	public:
		double R,G,B;

		Color(double r = 0, double g = 0, double b = 0)
			:R(r), G(g), B(b)
		{}
		Color(const Color& color)
			:R(color.R), G(color.G), B(color.B)
		{}

		void Clear() {R=0;G=0;B=0;}

		friend ostream& operator<<(ostream & out, Color & color);
};

Color operator*(const Color& c, double d);
Color operator*(double d, const Color& c);
Color operator*(const Color& c1, const Color& c2);
Color operator+(const Color& c1, const Color& c2);
Color operator/(const Color& c, double d);

#endif /* COLOR_H_ */
