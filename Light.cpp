#include "Light.h"
#include <cmath>

Vector3d & Light::GetPosition() { return m_Position; }

Color & Light::GetColor() { return m_Color; }

string Light::Save() {
	stringstream out;
	out << "[LIGHT]" << endl
	    << m_Position.x() << ','
	    << m_Position.y() << ','
	    << m_Position.z() << ','
	    << m_Color.R << ','
	    << m_Color.G << ','
	    << m_Color.B
	    << endl << "[/LIGHT]" << endl;

	return out.str();
}