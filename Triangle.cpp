#include <cmath>
#include "Triangle.h"

TraceResult Triangle::Intersect(Ray & a_Ray)
{
	TraceResult a_Result;
	a_Result.Hit = Miss;

	if (a_Ray.m_SpawnObject == this)
		return a_Result;

	Vector3d e1 = m_Vertices[1]->Position - m_Vertices[0]->Position;
	Vector3d e2 = m_Vertices[2]->Position - m_Vertices[0]->Position;
	Vector3d h = a_Ray.m_Direction.cross(e2);
	double a = e1.dot(h);

	//if (a == 0.0)
	if (a > -0.00000001 && a < 0.00000001)
		return a_Result;

	double f = 1.0 / a;

	Vector3d s = a_Ray.m_Origin - m_Vertices[0]->Position;
	double u = f * s.dot(h);

	if (u < 0.0 || u > 1.0)
		return a_Result;

	Vector3d q = s.cross(e1);
	double v = f * a_Ray.m_Direction.dot(q);
	if (v < 0.0 || u + v > 1.0)
		return a_Result;

	double t = f * e2.dot(q);

	if (t < 0.0)
		return a_Result;

	double gamma = 1.0 - u - v;

	a_Result.Hit = Hit;
	a_Result.Distance = t;
	a_Result.Intersection = a_Ray.m_Origin + (a_Ray.m_Direction * t);
	a_Result.Material = &m_Material;
	a_Result.Object = this;
	a_Result.Normal = GetInterpolatedNormal(gamma, u, v).normalized();
	SetTextureCoordinates(a_Result);

	return a_Result;
}

Vector3d Triangle::GetInterpolatedNormal(double alpha, double beta, double gamma)
{
	return (alpha * m_Vertices[0]->Normal) + (beta * m_Vertices[1]->Normal) + (gamma * m_Vertices[2]->Normal);
}

string Triangle::Save()
{
	stringstream out;
	out << "[TRIANGLE]" << endl
		<< m_Vertices[0]->Position.x() << ','
		<< m_Vertices[0]->Position.y() << ','
		<< m_Vertices[0]->Position.z() << ','
		<< m_Vertices[1]->Position.x() << ','
		<< m_Vertices[1]->Position.y() << ','
		<< m_Vertices[1]->Position.z() << ','
		<< m_Vertices[2]->Position.x() << ','
		<< m_Vertices[2]->Position.y() << ','
		<< m_Vertices[2]->Position.z() << ','
		<< m_Material.GetColor().R << ','
		<< m_Material.GetColor().G << ','
		<< m_Material.GetColor().B << ','
		<< m_Material.GetDiffuse() << ','
		<< m_Material.GetSpecular() << ','
		<< m_Material.GetSpecFalloff() << ','
		<< m_Material.GetReflectivity() << ','
		<< m_Material.GetRefraction() << ','
		<< m_Material.GetRefractionIndex() << ','
		<< m_Material.GetTexture().m_Filename << ','
		<< m_Material.GetTexture().m_XScale << ','
		<< m_Material.GetTexture().m_YScale << ','
		<< m_Material.GetTexture().m_Rotation << ','
		<< m_Material.GetTexture().m_Origin.x() << ','
		<< m_Material.GetTexture().m_Origin.y()
		<< endl << "[/TRIANGLE]" << endl;

	return out.str();
}
void Triangle::SetTextureCoordinates(TraceResult & a_Result)
{
	Texture texture = m_Material.GetTexture();

	//Position relative to first vertex
	Vector3d pos = a_Result.Intersection - m_Vertices[0]->Position;
	double u = pos.dot(m_UAxis) * texture.m_XScale;
	double v = pos.dot(m_VAxis) * texture.m_YScale;

	double uRot = u*cos(texture.m_Rotation) - v*sin(texture.m_Rotation);
	v = v*cos(texture.m_Rotation) + u*sin(texture.m_Rotation);
	u = uRot + texture.m_Origin.x();
	v = v + texture.m_Origin.y();

	a_Result.TextureCoordinate.u = u;
	a_Result.TextureCoordinate.v = v;
}