#include <iostream>
#include <sstream>
#include <string>
#include <chrono>
#include <thread>
#include "SDL.h"
#include "Scene.h"
#include "Renderer.h"
using namespace std;

// Globals
SDL_Renderer* sdlRenderer;

void PrintUsage();

void renderThread(unsigned int screenWidth, unsigned int screenHeight, unsigned int aa_samples, string sceneFile) {
	Scene newScene;
	if (!newScene.Load(sceneFile)) {
		return;
	}
	
	Uint32 ticks = SDL_GetTicks();

	Renderer rt(screenWidth, screenHeight, aa_samples);
	rt.Render(newScene, sdlRenderer);

	ticks = SDL_GetTicks() - ticks;

	//Output some render statistics
	cout << "Render Time: " << ticks / 1000.0
		 << " seconds @ " << screenWidth << "x" << screenHeight << endl;
}

int main(int argc, char** argv) {
	//renderer params
	bool fullscreen = false;
	bool display_lights = false;
	unsigned int screenWidth = 640;
	unsigned int screenHeight = 480;
	double fov = 90;
	string sceneFile = "";
	unsigned int aa_samples = 1;

	//create our event vars
	SDL_Event event;
	SDL_Event quit;
	quit.type = SDL_QUIT;

	string cmd_opt;
	//Check for cmd line options
	if (argc > 1) {
		for (int i = 1; i < argc; ++i) {
			cmd_opt = argv[i];

			if (cmd_opt == "--Fullscreen" || cmd_opt == "-f") {
				fullscreen = true;
			/*
			} else if (cmd_opt == "--Width" || cmd_opt == "-w") {
				++i;
				screenWidth = atoi(argv[i]);
			} else if (cmd_opt == "--Height" || cmd_opt == "-h") {
				++i;
				screenHeight = atoi(argv[i]);
			*/
			} else if (cmd_opt == "--Resolution" || cmd_opt == "-r") {
				++i;
				screenWidth = atoi(argv[i]);
				++i;
				screenHeight = atoi(argv[i]);
			} else if (cmd_opt == "--FieldOfView" || cmd_opt == "-fov") {
				++i;
				fov = atof(argv[i]);
			} else if (cmd_opt == "--Lights" || cmd_opt == "-l") {
				display_lights = true;
			} else if (cmd_opt == "--Scene" || cmd_opt == "-s") {
				++i;
				sceneFile = argv[i];
			} else if (cmd_opt == "--FSAA" || cmd_opt == "-aa") {
				++i;
				int temp = atoi(argv[i]);
				if (temp == 2 || temp == 4 || temp == 8 || temp == 16) {
					aa_samples = temp;
				} else {
					cout << "Number of samples must be 2, 4, 8 or 16"
					     << endl;
					return 0;
				}
			} else if (cmd_opt == "--Help" || cmd_opt == "-?") {
				PrintUsage();
				return 0;
			} else {
				cout << "Invalid option. Please type --Help or"
						" -? for available options" << endl;
				return 0;
			}
		}
	} else {
		PrintUsage();
		return 0;
	}

	//Initialize Video, exit on failure
	if (SDL_Init(SDL_INIT_VIDEO) == -1) {
			std::cout << "Failed to Initialize Video!";
			SDL_Quit();
			return 1;
	}

	//Create drawing surface
	SDL_Window* sdlWindow = SDL_CreateWindow(
		"PRTE Window",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		screenWidth, screenHeight,
		SDL_WINDOW_OPENGL | (fullscreen ? SDL_WINDOW_FULLSCREEN : 0));

	if (sdlWindow == NULL) {
		cout << "Could not create SDL window" << endl;
		SDL_Quit();
		return 1;
	}

	sdlRenderer = SDL_CreateRenderer(sdlWindow, -1, 0);

	if (sdlRenderer == NULL) {
		cout << "Could not create SDL renderer" << endl;
		SDL_Quit();
		return 1;
	}

	std::thread t(&renderThread, screenWidth, screenHeight, aa_samples, sceneFile);
	t.detach();

	//Just hang out until we hit escape
	do {
		if (SDL_PollEvent(&event)) {
			switch (event.key.state) {
				case SDL_RELEASED:
					switch (event.key.keysym.sym) {
						case SDLK_ESCAPE:
							SDL_PushEvent(&quit);
							break;
						default:
							break;
					}
					break;
				default:
					break;
			}
		}

		// Don't kill the processor
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	} while (event.type != SDL_QUIT);

	SDL_Quit();

	return 0;
}

void PrintUsage() {
	cout << "usage: PRtE [-f] [-w width] [-h height] [-r width height]\n"
			"            [-l] [-fov] [-s scenefile] [-?]"
		 << endl
		 << endl
		 << "\t--Fullscreen -f                     Enable fullscreen display"
		 << endl
		 //<< "\t--Width -w [width]                  Set screen width"
		 //<< endl
		 //<< "\t--Height -h [height]                Set screen height"
		 //<< endl
		 << "\t--Resolution -r [width] [height]    Set screen resolution"
		 << endl
		 //<< "\t--Lights -l                         Enable display of light sources"
		 //<< endl
		 //<< "\t--FieldOfView -fov [fov]            Set the camera field of view"
		 //<< endl
		 << "\t--Scene -s [scenefile]              Load the specified scene file"
		 << endl
		 << "\t--Help -?                           Display this help dialog"
		 << endl;
}
