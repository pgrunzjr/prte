#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <vector>
#include "RenderObject.h"
#include <Eigen\Dense>
using namespace Eigen;

struct Vertex
{
	Vector3d Position;
	Vector3d Normal;
	Color* Color = nullptr;

	Vertex(Vector3d position) : Position(position), NonNormal(0, 0, 0) {}
	void AddFaceNormal(const Vector3d& faceNormal)
	{
		NonNormal += faceNormal;
		Normal = NonNormal.normalized();
	};

private:
	Vector3d NonNormal;
};

class Triangle : public RenderObject
{
private:
	vector<Vertex*> m_Vertices = vector<Vertex*>(3);
	Vector3d m_UAxis, m_VAxis;
	Vector3d m_Normal;

	void SetTextureCoordinates(TraceResult & a_Result);
	Vector3d GetInterpolatedNormal(double alpha, double beta, double gamma);

public:
	Triangle(
		Vertex* a_Vertex1,
		Vertex* a_Vertex2,
		Vertex* a_Vertex3,
		const Material & a_Material = Material()
		) :
		RenderObject(a_Material)
	{
		m_Vertices[0] = a_Vertex1;
		m_Vertices[1] = a_Vertex2;
		m_Vertices[2] = a_Vertex3;
		m_Normal = (m_Vertices[1]->Position - m_Vertices[0]->Position)
			.cross(m_Vertices[2]->Position - m_Vertices[0]->Position)
			.normalized();

		m_UAxis = (m_Vertices[0]->Position - m_Vertices[1]->Position).normalized();
		m_VAxis = m_UAxis.cross(m_Normal);
	}
	~Triangle() {}

	const Vector3d& GetNormal()
	{
		return m_Normal;
	}
	virtual string Save();

	TraceResult Intersect(Ray & a_Ray);
};

#endif // TRIANGLE_H