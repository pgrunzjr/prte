#pragma once

#include <vector>
#include <Eigen/Dense>
#include "RenderObject.h"
#include "Triangle.h"
#include "Sphere.h"
#include "AABB.h"
using namespace Eigen;

class Mesh : public RenderObject
{
private:
	vector<Vertex> m_Vertices;
	vector<Triangle> m_Faces;
	Vector3d m_Maxima = Vector3d(0.0, 0.0, 0.0);
	Vector3d m_Minima = Vector3d(0.0, 0.0, 0.0);
	void UpdateBounds(double x, double y, double z);
	bool m_CanSelfShade;

public:
	Sphere* m_BoundingSphere = nullptr;
	AABB* m_AABB = nullptr;
	Mesh(const Material& a_Material = Material(), bool canSelfShade = false);
	~Mesh();

	void AddVertex(double x, double y, double z);
	void AddFace(int v1, int v2, int v3);
	void Mesh::CreateBoundingSphere();
	void Mesh::CreateAABB();
	TraceResult Intersect(Ray & a_Ray);
	string Save();
};