/*
 * scene.cpp
 *
 *  Created on: Aug 2, 2008
 *      Author: phildo
 */

#include "Scene.h"
#include "Triangle.h"
#include "Sphere.h"
#include "Plane.h"
#include "Mesh.h"

bool Scene::Load(string a_Filename)
{
	ifstream sceneIn;
	sceneIn.open(a_Filename.c_str());
	if (!sceneIn.is_open())
	{
		cout << "Error: Could not open file " << a_Filename << ". Please check the filename." << endl;

		return false;
	}

	//TODO Move all parsing into separate functions based on filetype
	if (a_Filename.find(".ply") != string::npos)
	{
		return LoadPLY(sceneIn);
	}

	string curLine;
	bool camLoaded = false;

	//Load Scene
	getline(sceneIn, curLine);
	for (int line = 1; !sceneIn.eof(); ++line, getline(sceneIn, curLine))
	{
		//@TODO Improve comments parsing
		string::size_type comPos = curLine.find('#');
		if (comPos == 0)
		{
			//do nothing
		}
		else
			if (curLine == "[CAMERA]")
			{
				for (
					++line, getline(sceneIn, curLine);
					!sceneIn.eof() && curLine != "[/CAMERA]";
					++line, getline(sceneIn, curLine)
					)
				{
					vector<string> vals = Explode(curLine, ',');

					if (vals.size() == 7)
					{
						m_Camera = Camera(
							Vector3d(
								atof(vals[0].c_str()),
								atof(vals[1].c_str()),
								atof(vals[2].c_str())
								),
							Vector3d(
								atof(vals[3].c_str()),
								atof(vals[4].c_str()),
								atof(vals[5].c_str())
								),
							atof(vals[6].c_str())
							);

						camLoaded = true;
					}
					else
					{
						cout << "Syntax Error: Invalid Camera definition at line "
							<< line << "." << endl << curLine << endl;

						return false;
					}
				}
			}
			else
				if (curLine == "[SPHERE]")
				{
					for (
						++line, getline(sceneIn, curLine);
						!sceneIn.eof() && curLine != "[/SPHERE]";
						++line, getline(sceneIn, curLine)
						)
					{
						vector<string> vals = Explode(curLine, ',');

						if (vals.size() == 14)
						{
							m_Primitives.push_back(new Sphere(
								Vector3d(
									atof(vals[0].c_str()),
									atof(vals[1].c_str()),
									atof(vals[2].c_str())
									),
								atof(vals[3].c_str()),
								Material(
									Color(
										atof(vals[4].c_str()),
										atof(vals[5].c_str()),
										atof(vals[6].c_str())
										),
									atof(vals[7].c_str()),
									atof(vals[8].c_str()),
									atof(vals[9].c_str()),
									atof(vals[10].c_str()),
									atof(vals[11].c_str()),
									atof(vals[12].c_str()),
									vals[13]
									)
								));
						}
						else
						{
							cout << "Syntax Error: Invalid Sphere definition at line "
								<< line << "." << endl << curLine << endl;

							return false;
						}
					}
				}
				else
					if (curLine == "[PLANE]")
					{
						for (
							++line, getline(sceneIn, curLine);
							!sceneIn.eof() && curLine != "[/PLANE]";
							++line, getline(sceneIn, curLine)
							)
						{
							vector<string> vals = Explode(curLine, ',');

							if (vals.size() == 21)
							{
								m_Primitives.push_back(new Plane(
									Vector3d(
										atof(vals[0].c_str()),
										atof(vals[1].c_str()),
										atof(vals[2].c_str())
										),
									atof(vals[3].c_str()),
									Material(
										Color(
											atof(vals[4].c_str()),
											atof(vals[5].c_str()),
											atof(vals[6].c_str())
											),
										atof(vals[7].c_str()),
										atof(vals[8].c_str()),
										atof(vals[9].c_str()),
										atof(vals[10].c_str()),
										atof(vals[11].c_str()),
										atof(vals[12].c_str()),
										Texture( // Texture Map
											vals[13],
											atof(vals[16].c_str()),
											atof(vals[17].c_str()),
											atof(vals[18].c_str()),
											Vector3d(
												atof(vals[19].c_str()),
												atof(vals[20].c_str()),
												0
												)
											),
										Texture( // Normal Map
											vals[14],
											atof(vals[16].c_str()),
											atof(vals[17].c_str()),
											atof(vals[18].c_str()),
											Vector3d(
												atof(vals[19].c_str()),
												atof(vals[20].c_str()),
												0
												)
											),
										Texture( // Specular Map
											vals[15],
											atof(vals[16].c_str()),
											atof(vals[17].c_str()),
											atof(vals[18].c_str()),
											Vector3d(
												atof(vals[19].c_str()),
												atof(vals[20].c_str()),
												0
												)
											)
										)
									));
							}
							else
							{
								cout << "Syntax Error: Invalid Plane definition at line "
									<< line << "." << endl << curLine << endl;

								return false;
							}
						}
					}
					else
						if (curLine == "[TRIANGLE]")
						{
							//for (
							//	++line, getline(sceneIn, curLine);
							//	!sceneIn.eof() && curLine != "[/TRIANGLE]";
							//	++line, getline(sceneIn, curLine)
							//	)
							//{
							//	if (curLine.find('#') != 0)
							//	{
							//		vector<string> vals = Explode(curLine, ',');

							//		if (vals.size() == 24)
							//		{
							//			Vector3d vertex[3] = {
							//				Vector3d(
							//					atof(vals[0].c_str()),
							//					atof(vals[1].c_str()),
							//					atof(vals[2].c_str())
							//				),
							//				Vector3d(
							//					atof(vals[3].c_str()),
							//					atof(vals[4].c_str()),
							//					atof(vals[5].c_str())
							//				),
							//				Vector3d(
							//					atof(vals[6].c_str()),
							//					atof(vals[7].c_str()),
							//					atof(vals[8].c_str())
							//				)
							//			};
							//			m_Primitives.push_back(new Triangle(
							//				vertex[0],
							//				vertex[1],
							//				vertex[2],
							//				Material(
							//					Color(
							//						atof(vals[9].c_str()),
							//						atof(vals[10].c_str()),
							//						atof(vals[11].c_str())
							//						),
							//					atof(vals[12].c_str()),
							//					atof(vals[13].c_str()),
							//					atof(vals[14].c_str()),
							//					atof(vals[15].c_str()),
							//					atof(vals[16].c_str()),
							//					atof(vals[17].c_str()),
							//					Texture(
							//						vals[18],
							//						atof(vals[19].c_str()),
							//						atof(vals[20].c_str()),
							//						atof(vals[21].c_str()),
							//						Vector3d(
							//							atof(vals[22].c_str()),
							//							atof(vals[23].c_str()),
							//							0
							//							)
							//						)
							//					)
							//				));
							//		}
							//		else
							//		{
							//			cout << "Syntax Error: Invalid Triangle definition at line "
							//				<< line << "." << endl << curLine << endl;

							//			return false;
							//		}
							//	}
							//}
						}
						else
							if (curLine == "[LIGHT]")
							{
								for (
									++line, getline(sceneIn, curLine);
									!sceneIn.eof() && curLine != "[/LIGHT]";
									++line, getline(sceneIn, curLine)
									)
								{
									vector<string> vals = Explode(curLine, ',');

									if (vals.size() == 6)
									{
										m_Lights.push_back(Light(
											Vector3d(
												atof(vals[0].c_str()),
												atof(vals[1].c_str()),
												atof(vals[2].c_str())
												),
											Color(
												atof(vals[3].c_str()),
												atof(vals[4].c_str()),
												atof(vals[5].c_str())
												)
											));
									}
									else
									{
										cout << "Syntax Error: Invalid Light definition at line "
											<< line << "." << endl << curLine << endl;

										return false;
									}
								}
							}
							else
							{
								cout << "Syntax Error: Expected section definition at line "
									<< line << "." << endl << curLine << endl;

								return false;
							}
	}

	if (!camLoaded)
	{
		cout << "No Camera definition was found. Please check the scene file." << endl;

		return false;
	}

	sceneIn.close();

	return true;
}

void Scene::Save(string a_Filename)
{
	//Open a file to output a scene file for the scene
	ofstream sceneOut;
	sceneOut.open(a_Filename.c_str());

	if (!sceneOut.is_open())
	{
		cout << "Error: Could not open file " << a_Filename
			<< ". Please check the filename." << endl;

		//return 0;
	}

	sceneOut << "[CAMERA]" << endl
		<< m_Camera.m_Position.x() << ','
		<< m_Camera.m_Position.y() << ','
		<< m_Camera.m_Position.z() << ','
		<< m_Camera.m_Target.x() << ','
		<< m_Camera.m_Target.y() << ','
		<< m_Camera.m_Target.z() << ','
		<< m_Camera.m_Fov << endl
		<< "[/CAMERA]" << endl;

	for (unsigned int m = 0; m < m_Primitives.size(); ++m)
	{
		sceneOut << m_Primitives[m]->Save();
	}
	for (unsigned int m = 0; m < m_Lights.size(); ++m)
	{
		sceneOut << m_Lights[m].Save();
	}

	sceneOut.close();
}

vector<string> Scene::Explode(string str, char delim)
{
	vector<string> vec;

	for (string::size_type i = str.find(delim); i != str.npos; i = str.find(delim))
	{
		vec.push_back(str.substr(0, i));
		str = str.substr(i + 1);
	}
	if (str.size() > 0)
	{
		vec.push_back(str);
	}

	return vec;
}

// TODO This needs to be expanded to full support .ply files
bool Scene::LoadPLY(ifstream& a_File)
{
	string curLine;
	int numVerts = 0;
	int numFaces = 0;
	getline(a_File, curLine);
	for (int line = 1; !a_File.eof() && curLine != "end_header"; ++line, getline(a_File, curLine))
	{
		if (curLine.find("element vertex ") != string::npos)
		{
			numVerts = atoi(curLine.substr(curLine.rfind(" ") + 1).c_str());
			cout << "V: " << numVerts << endl;
		}
		if (curLine.find("element face ") != string::npos)
		{
			numFaces = atoi(curLine.substr(curLine.rfind(" ") + 1).c_str());
			cout << "F: " << numFaces << endl;
		}
	}

	Vector3d* verts = new Vector3d[numVerts];

	//Mesh* mesh = new Mesh(Material(Color(0.737255, 0.0784314, 0.784314), 1, 0.9, 300, .4, 1, .33));
	Mesh* mesh = new Mesh(Material(Color(1, 1, 1), 1, 0, 0, 0, 0, 0), true);
	m_Primitives.push_back(mesh);

	getline(a_File, curLine);
	for (int i = 0; i < numVerts; ++i, getline(a_File, curLine))
	{
		vector<string> stuff = Explode(curLine, ' ');
		mesh->AddVertex(atof(stuff[0].c_str()), atof(stuff[1].c_str()), atof(stuff[2].c_str()));
	}

	for (int i = 0; i < numFaces; ++i, getline(a_File, curLine))
	{
		vector<string> stuff = Explode(curLine, ' ');
		mesh->AddFace(atoi(stuff[1].c_str()), atoi(stuff[2].c_str()), atoi(stuff[3].c_str()));
	}

	mesh->CreateAABB();

	//1,0.9,300,.4,0,.33, 
	m_Primitives.push_back(
		new Plane(
			Vector3d(0, 0, 1), -3,
			Material(
				Color(1, 1, 1),
				1, 0.2, 10, 0, 0, 0,
				Texture("WTX_GK_001_cc.bmp", 500, 500, 0, Vector3d(0, 0, 0)))));

	m_Lights.push_back(Light(Vector3d(0, 2, .5), Color(1, 0, 0)));
	m_Lights.push_back(Light(Vector3d(-1.25, -2, .5), Color(0, 1, 0)));
	m_Lights.push_back(Light(Vector3d(1.25, -2, .5), Color(0, 0, 1)));
	m_Camera = Camera(Vector3d(0, .13, .3 ), Vector3d(0, 0, -1), 90);

	return true;
}
