#ifndef RAYTRACER_H_
#define RAYTRACER_H_

#include "SDL.h"
#include "Color.h"
#include "Ray.h"
#include "Scene.h"
#include "Light.h"
#include "RenderObject.h"
using namespace std;

#define PI 3.14159265
#define E 2.71828183
#define MAX_DEPTH 3

class RayTracer {
	Scene m_Scene;
	unsigned int m_Samples;

	private:
		Color Shade(
			TraceResult & a_HitResult,
			Ray* a_Ray,
			double a_RefIndex,
			double a_Ambiant
		);

	public:
		RayTracer(unsigned int a_Samples, const Scene & a_Scene)
			:m_Samples(a_Samples), m_Scene(a_Scene)
		{}
		~RayTracer() {
		}

		Color Trace(
			Ray* a_Ray,
			Vector3d & a_Intersect = Vector3d(0, 0, 0),
			double a_RefIndex = 1.0f
			);
};

#endif /* RAYTRACER_H_ */
