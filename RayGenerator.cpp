#include "RayGenerator.h"

RayGenerator::RayGenerator(unsigned int a_ScreenWidth, unsigned int a_ScreenHeight, unsigned int a_Samples, const Scene & a_Scene)
	: m_ScreenWidth(a_ScreenWidth), m_ScreenHeight(a_ScreenHeight),	m_Samples(a_Samples), m_Scene(a_Scene)
{
	double fov = m_Scene.m_Camera.m_Fov * (PI / 180.0);

	//Setup our camera axis
	Vector3d z_axis = (m_Scene.m_Camera.m_Position - m_Scene.m_Camera.m_Target).normalized();

	//This is a very poor camera system that will break if the angle of
	//rotation about x is -90 or 90. Cross product
	Vector3d x_axis = Vector3d(0, 1, 0).cross(z_axis);
	Vector3d y_axis = x_axis.cross(-1 * z_axis);

	//These axes make up our rotation matrix
	m_Rotation(0, 0) = x_axis.x();
	m_Rotation(1, 0) = x_axis.y();
	m_Rotation(2, 0) = x_axis.z();
	m_Rotation(0, 1) = y_axis.x();
	m_Rotation(1, 1) = y_axis.y();
	m_Rotation(2, 1) = y_axis.z();
	m_Rotation(0, 2) = z_axis.x();
	m_Rotation(1, 2) = z_axis.y();
	m_Rotation(2, 2) = z_axis.z();
	m_Rotation = m_Rotation.inverse().eval();

	double ratio = (double)m_ScreenHeight / (double)m_ScreenWidth;
	double cam_width = 0.033f;
	double cam_height = cam_width * ratio;
	m_PixelWidth = (double)cam_width / (double)m_ScreenWidth;
	m_PixelHeight = (double)cam_height / (double)m_ScreenHeight;
	m_RayWidth = m_PixelWidth / m_Samples;
	m_RayHeight = m_PixelHeight / m_Samples;
	m_MaxXCoordinate = m_ScreenWidth / 2;
	m_MaxYCoordinate = m_ScreenHeight / 2;
	m_NumberOfRaysPerPixel = m_Samples * m_Samples;

	//Find eye offset, rotate, then translate
	m_Scene.m_Camera.m_Eye = Vector3d(0, 0, 0);
	m_Scene.m_Camera.m_Eye(2) = (sqrt(pow(cam_width, 2) + pow(cam_height, 2)) / tan(fov / 2));
	m_Scene.m_Camera.m_Eye = m_Scene.m_Camera.m_Eye.transpose() * m_Rotation;
	m_Scene.m_Camera.m_Eye = m_Scene.m_Camera.m_Eye + m_Scene.m_Camera.m_Position;
}

RayGenerator::~RayGenerator()
{
}

Ray* RayGenerator::GenerateRays(unsigned int a_PixelXCoordinate, unsigned int a_PixelYCoordinate)
{
	Ray* rays = new Ray[m_NumberOfRaysPerPixel];
	Ray* ray = rays;

	for (unsigned int x = 0; x < m_Samples; x++) {
		for (unsigned int y = 0; y < m_Samples; y++) {
			// Here we calculate all of our directions starting from the origin
			// Then we rotate them, and translate by the camera position
			ray->m_Direction(0) = ((a_PixelXCoordinate - m_MaxXCoordinate) * m_PixelWidth) + (m_RayWidth * x);
			ray->m_Direction(1) = ((m_MaxYCoordinate - a_PixelYCoordinate) * m_PixelHeight) + (m_RayHeight * y);

			ray->m_Direction(2) = 0;
			ray->m_Direction = ray->m_Direction.transpose() * m_Rotation;
			ray->m_Direction = ray->m_Direction + m_Scene.m_Camera.m_Position;

			// Set ray origin to projection screen, so objects between eye
			// and screen are not drawn
			ray->m_Origin = ray->m_Direction;

			//Normalize the ray direction
			ray->m_Direction = (ray->m_Direction - m_Scene.m_Camera.m_Eye).normalized();
			ray++;
		}
	}

	return rays;
}
