#ifndef RAY_H
#define RAY_H

#include <Eigen\Dense>
using namespace Eigen;

class RenderObject;
class Ray
{
public:
	Vector3d m_Origin, m_Direction;
	RenderObject* m_SpawnObject;
	int m_Depth;

	Ray(
		Vector3d a_Origin = Vector3d(0, 0, 0),
		Vector3d a_Direction = Vector3d(0, 0, 0),
		int a_Depth = 0,
		RenderObject* a_SpawnObject = nullptr)
		: m_Origin(a_Origin), m_Direction(a_Direction), m_Depth(a_Depth), m_SpawnObject(a_SpawnObject)
	{}

	~Ray() {}
};

//@TODO Temporary location, needs to be moved into its own headers
class Camera
{
public:
	Vector3d m_Position;
	Vector3d m_Eye;
	Vector3d m_Target;
	double m_Fov;


	Camera(
		Vector3d a_Position = Vector3d(0, 0, -2),
		Vector3d a_Target = Vector3d(0, 0, 1),
		double a_Fov = 90)
		: m_Position(a_Position), m_Target(a_Target), m_Fov(a_Fov), m_Eye(a_Position)
	{}

	~Camera() {}
};
#endif
