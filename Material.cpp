/*
 * material.cpp
 */

#include <iostream>
#include <cmath>
#include "Material.h"

/*
 * Material
 */
//Accessors
Color Material::GetColor() { return m_Color; }
double Material::GetDiffuse() { return m_Diffuse; }
double Material::GetSpecular() { return m_Specular; }
double Material::GetSpecFalloff() { return m_SpecFalloff; }
double Material::GetReflectivity() { return m_Reflectivity; }
double Material::GetRefraction() { return m_Refraction; }
double Material::GetRefractionIndex() { return m_RefractionIndex; }
Texture& Material::GetTexture() { return m_Texture; }
Texture& Material::GetNormalMap() { return m_NormalMap; }
Texture& Material::GetSpecularMap() { return m_SpecularMap; }

//Mutators
void Material::SetColor(Color a_Color) { m_Color = a_Color; }
void Material::SetDiffuse(double a_Diffuse) { m_Diffuse = a_Diffuse; }
void Material::SetSpecular(double a_Specular) { m_Specular = a_Specular; }
void Material::SetSpecFalloff(double a_SpecFalloff) { m_SpecFalloff = a_SpecFalloff; }
void Material::SetReflectivity(double a_Reflectivity) { m_Reflectivity = a_Reflectivity; }

/*
 * Texture
 */
Uint32 Texture::GetTexel(int u, int v)
{
	while (u > m_Image->w - 1) {
		u-= m_Image->w;
	}
	while (v > m_Image->h - 1) {
		v -= m_Image->h;
	}
	while (u < 0) {
		u += m_Image->w;
	}
	while (v < 0) {
		v += m_Image->h;
	}

	SDL_LockSurface(m_Image);

	int bpp = m_Image->format->BytesPerPixel;
	/* Here p is the address to the pixel we want to retrieve */
	Uint8 *p = (Uint8 *)m_Image->pixels + v * m_Image->pitch + u * bpp;

	SDL_UnlockSurface(m_Image);

    switch(bpp) {
    case 1:
        return *p;

    case 2:
        return *(Uint16 *)p;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;

    case 4:
        return *(Uint32 *)p;

    default:
        return 0;       /* shouldn't happen, but avoids warnings */
    }
}

//Standard nearest neighbor filtering
Color Texture::GetPixColor(const TextureCoordinate& coordinate)
{
	return GetPixColor(coordinate.u, coordinate.v);
}
Color Texture::GetPixColor(double U, double V) {
	int Ui = (int) round(U);
	int Vi = (int) round(V);

	Uint8 r,g,b;

	SDL_GetRGB(GetTexel(Ui, Vi), m_Image->format, &r, &g, &b);
	Color ret_val = Color(r/255.0f, g/255.0f, b/255.0f);

	return ret_val;
}

//Bilinear interpolation filtering
Color Texture::GetBilerpPixColor(const TextureCoordinate& coordinate)
{
	return GetBilerpPixColor(coordinate.u, coordinate.v);
}
Color Texture::GetBilerpPixColor(double U, double V) {
	int Ui = (int) floor(U);
	int Vi = (int) floor(V);

	double Uf = U - Ui;
	double Vf = V - Vi;

	double TL = (1.0f - Uf) * (1.0f - Vf);
	double TR = Uf * (1.0f - Vf);
	double BL = (1.0f - Uf) * Vf;
	double BR = Uf * Vf;

	Uint8 r,g,b;

	SDL_GetRGB(GetTexel(Ui, Vi), m_Image->format, &r, &g, &b);
	Color cTL = Color(r/255.0f, g/255.0f, b/255.0f);

	SDL_GetRGB(GetTexel(Ui+1, Vi), m_Image->format, &r, &g, &b);
	Color cTR = Color(r/255.0f, g/255.0f, b/255.0f);

	SDL_GetRGB(GetTexel(Ui, Vi+1), m_Image->format, &r, &g, &b);
	Color cBL = Color(r/255.0f, g/255.0f, b/255.0f);

	SDL_GetRGB(GetTexel(Ui+1, Vi+1), m_Image->format, &r, &g, &b);
	Color cBR = Color(r/255.0f, g/255.0f, b/255.0f);

	Color ret_val = (cTL * TL) + (cTR * TR) + (cBL * BL) + (cBR * BR);

	return ret_val;
}
