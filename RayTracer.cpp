#include <cmath>
#include "Raytracer.h"
#include "Random.h"
#include "RayGenerator.h"
using namespace std;

Color RayTracer::Trace(Ray* ray, Vector3d & intersect, double a_RefIndex)
{
	// Don't perform multisampling on secondary rays
    unsigned int num_samples = (ray->m_Depth > 0 ? 1 : m_Samples * m_Samples);

    Color ret_color(0,0,0);

    for (unsigned int j = 0; j < num_samples; ++j, ray++) {
		RenderObject* nearest_prim = 0;

        //Vector to hold the intersect
        //bool is_intersection = false;
        bool is_light = false;
        TraceResult hit_result;
		hit_result.Hit = Miss;

        for (unsigned int cur_prim = 0; cur_prim < m_Scene.m_Primitives.size(); ++cur_prim) {
			TraceResult hit = m_Scene.m_Primitives[cur_prim]->Intersect(*ray);

            if (hit.Hit == Hit) {
                if (hit.Distance < hit_result.Distance || hit_result.Hit == Miss) {
                    hit_result = hit;
                    nearest_prim = m_Scene.m_Primitives[cur_prim];
                }
            }
        }

        if (hit_result.Hit == Hit) {
			intersect = hit_result.Intersection;
			ret_color = ret_color + Shade(hit_result, ray, a_RefIndex, .15);
        } else {
			ret_color = ret_color + Color(0.7882352941176471f, 0.8784313725490196f, 0.992156862745098f);
        }
    }

    return ret_color;
}

Color RayTracer::Shade(
    TraceResult & a_HitResult,
    Ray* a_Ray,
    double a_RefIndex,
    double a_Ambient)
{
    Color specular, diffuse, ambient, reflect;
	Color pixelColor;

	if (a_HitResult.Material->GetTexture().m_Image)
	{
		pixelColor = a_HitResult.Material->GetTexture().GetBilerpPixColor(a_HitResult.TextureCoordinate);
	}
	else
	{
		pixelColor = a_HitResult.Material->GetColor();
	}

    //Get the primitive surface normal
	// TODO Implement normal mapping
	Vector3d N = a_HitResult.Normal;

	// Ghetto normal mapping
	if (N.x() == 0 && N.y() == 0 && N.z() == 1) {
		Texture normal_map = a_HitResult.Material->GetNormalMap();
		if (normal_map.m_Image) {
			Color normal_color = normal_map.GetBilerpPixColor(a_HitResult.TextureCoordinate);
			N = Vector3d(-2.0 * (normal_color.R - 0.5), 2.0 * (normal_color.G - 0.5), 2.0 * (normal_color.B - 0.5)).normalized();
		}
	}

    //Calculate reflection normal
    Vector3d reflect_normal = a_Ray->m_Direction - (2 * (N.dot(a_Ray->m_Direction) * N));

    //Calculate ambient light component
	ambient = a_Ambient * pixelColor;

    for (unsigned int l = 0; l < m_Scene.m_Lights.size(); l++) {
        //Shadow ray
        //@TODO Move ray origin by EPSILON value to prevent intersections at origin
        Vector3d light_vector = (m_Scene.m_Lights[l].GetPosition()- a_HitResult.Intersection);
        Vector3d light_norm = light_vector.normalized();
		Color light_color = m_Scene.m_Lights[l].GetColor();
        Ray shadow_ray = Ray(a_HitResult.Intersection + (.001 * light_norm), light_norm, a_Ray->m_Depth, a_HitResult.Object);
        bool is_shadow = false;

        //Calculate distance between intersection and light source
        double light_distance = light_vector.norm();

        for (unsigned int cur_prim = 0; cur_prim < m_Scene.m_Primitives.size() && !is_shadow; ++cur_prim)
		{
			TraceResult hit = m_Scene.m_Primitives[cur_prim]->Intersect(shadow_ray);
			if (hit.Hit != Miss && hit.Distance < light_distance)
				is_shadow = true;
        }

        if (!is_shadow) {
            //standard attenuation equation TODO Which is? Put it in the comment ass!
            double light_intensity = 1.0f / (1 + (0.01f * light_distance) + (0.04f * light_distance * light_distance));

            //We take the dot product of our unit vectors to find the Cos of the
            //angle between the two vectors
            double shade = light_norm.dot(N) * (a_HitResult.Material->GetDiffuse() - a_Ambient);
            if (shade > 0) {
                diffuse = diffuse + (shade * (light_color * light_intensity) * pixelColor);
            }

            double spec = light_norm.dot(reflect_normal);
            if (spec > 0) {
				// TODO: Update this section to allow use of specular maps
				spec = pow(spec, a_HitResult.Material->GetSpecFalloff()) * a_HitResult.Material->GetSpecular();

                specular = specular + (spec * light_color * pixelColor);
            }
        }
    }

    //Calculate our base intensity
	pixelColor = ambient + specular + diffuse;

    //Do we need to generate secondary rays, or are we too deep?
    if (a_Ray->m_Depth < MAX_DEPTH) {
        //Do reflections?
		double reflectivity = a_HitResult.Material->GetReflectivity();
        if (reflectivity > 0) {
            Ray reflected_ray = Ray(a_HitResult.Intersection + (.001 * reflect_normal), reflect_normal, a_Ray->m_Depth + 1);
            Vector3d temp_intersect;
            Color temp_color = Trace(&reflected_ray, temp_intersect);
			pixelColor = (pixelColor * (1 - reflectivity)) + (temp_color * reflectivity);
        }

        //Do refractions?
		double refraction = a_HitResult.Material->GetRefraction();
        if (refraction > 0) {
            double refraction_index = a_HitResult.Material->GetRefractionIndex();
            double dist = 0;
            double C = 0.15f;

            if (a_HitResult.Hit == Inside) {
                refraction_index = 1.0f;
                dist = 0;
                C = 0;
            }

            double n = a_RefIndex / refraction_index;

			// TODO Implement normal mapper here.
            N = a_HitResult.Normal * a_HitResult.Hit;

            double cosI = N.dot(a_Ray->m_Direction);
            double cosT2 = 1.0f - ( (n * n) * (1.0f - (cosI * cosI)) );

            if (cosT2 > 0.0f) {
                double Tdist = ((-n * cosI) - sqrt(cosT2));
                Vector3d T = (n * a_Ray->m_Direction) + (Tdist * N);
                Ray refracted_ray = Ray(a_HitResult.Intersection + (.001 * T), T, a_Ray->m_Depth + 1);
                Vector3d temp_intersect;
                Color temp_color = Trace(&refracted_ray, temp_intersect, a_HitResult.Material->GetRefractionIndex());
                dist = (temp_intersect - a_HitResult.Intersection).norm();

                //Apply a rough approximation of Beer's law
                pixelColor = pixelColor + (temp_color * pow(E, -(dist * C)));
            }
        }
    }

    return pixelColor;
}
