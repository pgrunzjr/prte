#ifndef LIGHT_H
#define LIGHT_H

#include "Color.h"
#include <Eigen/Dense>
using namespace Eigen;

class Light {
	private:
		Vector3d m_Position;
		Color m_Color;

	public:

		Light(
			const Vector3d & a_Position = Vector3d(),
			const Color & a_Color = Color(1, 1, 1)
		):
			m_Position(a_Position), m_Color(a_Color)
		{}
		~Light() {}

		Vector3d & GetPosition();
		Color & GetColor();
		string Save();
};

#endif // LIGHT_H