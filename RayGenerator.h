#pragma once

#include "Ray.h";
#include "Scene.h";
#include <Eigen\Dense>
using namespace Eigen;

#define PI 3.14159265

class RayGenerator
{
	private:
		Scene m_Scene;
		unsigned int m_ScreenWidth, m_ScreenHeight;
		unsigned int m_Samples;
		unsigned int m_NumberOfRaysPerPixel;
		double m_PixelWidth;
		double m_PixelHeight;
		double m_RayWidth;
		double m_RayHeight;
		double m_MaxXCoordinate;
		double m_MaxYCoordinate;
		Matrix3d m_Rotation;

	public:
		RayGenerator(unsigned int a_ScreenWidth, unsigned int a_ScreenHeight, unsigned int a_Samples, const Scene & a_Scene);
		~RayGenerator();
		Ray* GenerateRays(unsigned int a_PixelXCoordinate, unsigned int a_PixelYCoordinate);
};

