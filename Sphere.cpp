#include "Sphere.h"

TraceResult Sphere::Intersect(Ray & a_Ray)
{
	TraceResult a_Result;
	a_Result.Hit = Miss;
	/*
	* Take the dot product of the ray direction, and the vector
	* distance from the ray origin to the sphere center;
	*/
	Vector3d EO = m_Center - a_Ray.m_Origin;
	double v = EO.dot(a_Ray.m_Direction);
	double disc = pow(m_Radius, 2) - (EO.dot(EO) - pow(v, 2));

	//ray does not intersect with sphere
	if (disc > 0)
	{
		disc = sqrt(disc);
		double intersect1 = v - disc;
		double intersect2 = v + disc;
		if (intersect2 > 0.0f)
		{
			//Sphere is in front of ray origin
			if (intersect1 < 0.0f)
			{
				//ray hits from inside sphere (refraction)
				a_Result.Hit = Inside;
				a_Result.Distance = intersect2;
				a_Result.Intersection = a_Ray.m_Origin + intersect2 * a_Ray.m_Direction;
			}
			else
			{
				a_Result.Hit = Hit;
				a_Result.Distance = intersect1;
				a_Result.Intersection = a_Ray.m_Origin + intersect1 * a_Ray.m_Direction;
			}

			a_Result.Normal = (a_Result.Intersection - m_Center).normalized();
			a_Result.Material = &m_Material;
			SetTextureCoordinates(a_Result);
		}
	}

	return a_Result;
}
string Sphere::Save()
{
	stringstream out;
	out << "[SPHERE]" << endl
		<< m_Center.x() << ','
		<< m_Center.y() << ','
		<< m_Center.z() << ','
		<< m_Radius << ','
		<< m_Material.GetColor().R << ','
		<< m_Material.GetColor().G << ','
		<< m_Material.GetColor().B << ','
		<< m_Material.GetDiffuse() << ','
		<< m_Material.GetSpecular() << ','
		<< m_Material.GetSpecFalloff() << ','
		<< m_Material.GetReflectivity() << ','
		<< m_Material.GetRefraction() << ','
		<< m_Material.GetRefractionIndex() << ','
		<< m_Material.GetTexture().m_Filename << ','
		<< m_Material.GetTexture().m_XScale << ','
		<< m_Material.GetTexture().m_YScale << ','
		<< m_Material.GetTexture().m_Rotation << ','
		<< m_Material.GetTexture().m_Origin.x() << ','
		<< m_Material.GetTexture().m_Origin.y()
		<< endl << "[/SPHERE]" << endl;

	return out.str();
}
void Sphere::SetTextureCoordinates(TraceResult & a_Result)
{
	Vector3d m_UAxis = Vector3d(a_Result.Normal.y(), a_Result.Normal.z(), -a_Result.Normal.x());
	Vector3d m_VAxis = m_UAxis.cross(a_Result.Normal);

	Texture texture = m_Material.GetTexture();

	//Convert intersect coords to texture coords
	a_Result.TextureCoordinate.u = a_Result.Intersection.dot(m_UAxis) * texture.m_XScale;
	a_Result.TextureCoordinate.v = a_Result.Intersection.dot(m_VAxis) * texture.m_YScale;
}