#include "AABB.h"


AABB::AABB(Vector3d a_Maxima, Vector3d a_Minima) : m_Maxima(a_Maxima), m_Minima(a_Minima)
{}

AABB::AABB()
{}

AABB::~AABB()
{}

bool AABB::Intersect(const Ray& a_Ray)
{
	double enter = 0.0;
	double exit = DBL_MAX;

	for (size_t i = 0; i < 3; i++)
	{
		double inverseRayDirection = 1.0 / a_Ray.m_Direction(i);
		double near, far;

		if (inverseRayDirection < 0.0)
		{
			near = (m_Maxima(i) - a_Ray.m_Origin(i)) * inverseRayDirection;
			far = (m_Minima(i) - a_Ray.m_Origin(i)) * inverseRayDirection;
		}
		else
		{
			near = (m_Minima(i) - a_Ray.m_Origin(i)) * inverseRayDirection;
			far = (m_Maxima(i) - a_Ray.m_Origin(i)) * inverseRayDirection;
		}

		enter = near > enter ? near : enter;
		exit = far < exit ? far : exit;

		if (enter > exit)
			return false;
	}

	return true;
}