#ifndef RENDERER_H_
#define RENDERER_H_

#include "SDL.h"
#include "Scene.h"

class Renderer {
	private:
		unsigned int m_ScreenWidth, m_ScreenHeight;
		unsigned int m_Samples;

	public:
		Renderer(unsigned int a_ScreenWidth, unsigned int a_ScreenHeight, unsigned int a_Samples)
			:m_ScreenWidth(a_ScreenWidth), m_ScreenHeight(a_ScreenHeight), m_Samples(a_Samples)
		{}
		~Renderer() {
		}

		void Render(const Scene & a_Scene, SDL_Renderer* a_Screen);
};

#endif // RENDERER_H_