/*
 * material.h
 */

#ifndef MATERIAL_H_
#define MATERIAL_H_

#include <string>
#include <iostream>
#include "SDL.h"
#include "Color.h"
#include <Eigen\Dense>
using namespace Eigen;

#define PI 3.14159265

struct TextureCoordinate
{
	double u;
	double v;
};

//@TODO Would be beneficial to remove SDL dependency for texture functions
class Texture {
	public:
		SDL_Surface* m_Image;
		string m_Filename;
		double m_XScale;
		double m_YScale;
		double m_Rotation;
		Vector3d m_Origin;

		Texture(
			string a_Filename = " ",
			double a_XScale = 1,
			double a_YScale = 1,
			double a_Rotation = 0,
			const Vector3d & a_Origin = Vector3d(0,0,0)
		):
			m_Filename(a_Filename),
			m_XScale(a_XScale),
			m_YScale(a_YScale),
			m_Rotation(a_Rotation),
			m_Origin(a_Origin)
		{
			m_Image = SDL_LoadBMP(m_Filename.c_str());
			m_Rotation = m_Rotation * (PI / 180.0f);
		}

		/*
		 * Return the pixel value at (x, y)
		 * This is a mildly modified version of one I found gamedev.net forums
		 */
		Uint32 GetTexel(int u, int v);
		Color GetPixColor(double u, double v);
		Color GetBilerpPixColor(double u, double v);
		Color GetPixColor(const TextureCoordinate& coordinate);
		Color GetBilerpPixColor(const TextureCoordinate& coordinate);

};

class Material {
	private:
		Color m_Color;
		double m_Diffuse, m_Specular, m_SpecFalloff, m_Reflectivity;
		double m_Refraction, m_RefractionIndex;
		Texture m_Texture;
        Texture m_NormalMap;
        Texture m_SpecularMap;
	public:
		Material(
			const Color & a_Color = Color(1,1,1),
			double a_Diffuse = 1,
			double a_Specular = 0,
			double a_SpecFalloff = 50,
			double a_Reflectivity = 0,
			double a_Refraction = 0,
			double a_RefractionIndex = 1.33,
			const Texture& a_Texture = Texture(),
                        const Texture& a_NormalMap = Texture(),
                        const Texture& a_SpecularMap = Texture()
		):
			m_Color(a_Color),
			m_Diffuse(a_Diffuse),
			m_Specular(a_Specular),
			m_SpecFalloff(a_SpecFalloff),
			m_Reflectivity(a_Reflectivity),
			m_Refraction(a_Refraction),
			m_RefractionIndex(a_RefractionIndex),
			m_Texture(a_Texture),
                        m_NormalMap(a_NormalMap),
                        m_SpecularMap(a_SpecularMap)
		{}
		~Material(){}

		//Accessors
		Color GetColor();
		double GetDiffuse();
		double GetSpecular();
		double GetSpecFalloff();
		double GetReflectivity();
		double GetRefraction();
		double GetRefractionIndex();
		Texture& GetTexture();
		Texture& GetNormalMap();
		Texture& GetSpecularMap();


		//Mutators
		void SetColor(Color a_Color);
		void SetDiffuse(double a_Diffuse);
		void SetSpecular(double a_Specular);
		void SetSpecFalloff(double a_SpecFalloff);
		void SetReflectivity(double a_Reflectivity);
};

#endif /* MATERIAL_H_ */
